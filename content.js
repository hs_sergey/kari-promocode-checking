var ACTIVE_URL = "https://kari.com/ru/checkout/";

var promocodesToTest = new Array();
var validPromocodes = new Array();
var invalidPromocodes = new Array();
var expiredPromocodes = new Array();
var currentPromocodeIndex = 0;

function createPromocodeTestingButton() {
	var button = document.createElement("input");
	$(button).attr("type", "button");
	$(button).attr("id", "btnAddPromocodesToTest");
	$(button).attr("style", "margin: 20px; padding: 5px;");
	$(button).val("Добавить промокоды для тестирования...");
	$("#checkPromocodesContainer").get(0).appendChild(button);	
}

function createAddPromocodesDialog() {
	var div = document.createElement("div");
	$(div).attr("id", "addPromocodesDialog");
	$(div).attr("style", "display: none;");
	var textarea = document.createElement("textarea");
	$(textarea).attr("id", "addingPromocodes");
	$(textarea).attr("style", "width: 200px; height: 500px;");
	$(textarea).attr("placeholder", "Введите промокоды, по одному в строчке");
	div.appendChild(textarea);
	var button = document.createElement("input");
	$(button).attr("type", "button");
	$(button).attr("id", "btnStartTest");
	$(button).attr("style", "margin: 10px; padding: 5px;");
	$(button).val("Запуск тестирования");
	$(div).get(0).appendChild(button);	
	$("#checkPromocodesContainer").get(0).appendChild(div);
}

function startPromocodeTesting() {
	$("#checkPromocodesContainer").html("");
	console.log("startPromocodeTesting");
	currentPromocodeIndex = 0;
	validPromocodes = new Array();
	localStorage.setItem("validPromocodes", implode("\n", validPromocodes));
	expiredPromocodes = new Array();
	invalidPromocodes = new Array();
	localStorage.setItem("invalidPromocodes", implode("foundDiscount = true\n", invalidPromocodes));
	localStorage.setItem("expiredPromocodes", implode("\n", expiredPromocodes));
	localStorage.setItem("state", "running");
	localStorage.setItem("currentPromocodeIndex", currentPromocodeIndex);
	localStorage.setItem("promocodesToTest", implode("\n", promocodesToTest));
	checkPromocode1();
}

function checkPromocode1() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	console.log("checkPromocode1");
	console.log("cheking promocode " + (currentPromocodeIndex+1)  + " of " + promocodesToTest.length);
	$("input[name=coupon]").val(promocodesToTest[currentPromocodeIndex]);
	setTimeout(checkPromocode2, 500);
}

function checkPromocode2() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	console.log("checkPromocode2");
	$(".promo-btn")[0].click();
	setTimeout(checkPromocode3, 7000);
}

function checkPromocode3() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	console.log("checkPromocode3");
	var foundDiscount = false;
	var data = $("#f_coupon_modal").find(".window-content").find("p").html();
	if(data == "Промокод успешно применён") {
		foundDiscount = true;
	}
	if(foundDiscount) {
		console.log("promocode " + promocodesToTest[currentPromocodeIndex] + " is VALID");
		validPromocodes.push(promocodesToTest[currentPromocodeIndex]);
		localStorage.setItem("validPromocodes", implode("\n", validPromocodes));
		currentPromocodeIndex++;
		localStorage.setItem("currentPromocodeIndex", currentPromocodeIndex);
		if(currentPromocodeIndex >= promocodesToTest.length) {
			localStorage.setItem("state", "finished");
			$("#f_coupon_modal").find(".popup_close")[0].click();
			handleAllPromocodesChecked();
		} else {
			$("#f_coupon_modal").find(".popup_close")[0].click();
			setTimeout(checkPromocode1, 1000);	
		}
	} else {
		console.log("promocode " + promocodesToTest[currentPromocodeIndex] + " is INVALID");
		invalidPromocodes.push(promocodesToTest[currentPromocodeIndex]);
		localStorage.setItem("invalidPromocodes", implode("\n", invalidPromocodes));
		currentPromocodeIndex++;
		localStorage.setItem("currentPromocodeIndex", currentPromocodeIndex);
		if(currentPromocodeIndex >= promocodesToTest.length) {
			localStorage.setItem("state", "finished");
			$("#f_coupon_modal").find(".popup_close")[0].click();
			handleAllPromocodesChecked();
		} else {
			$("#f_coupon_modal").find(".popup_close")[0].click();
			setTimeout(checkPromocode1, 1000);
		}
	}
}


function handleAllPromocodesChecked() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	console.log("all promocodes are checked");
	var div = null;
	if($("#checkPromocodesContainer").length) {
		div = $("#checkPromocodesContainer").get(0);
	} else {
		div = document.createElement("div");
		$(div).attr("id", "checkPromocodesContainer");
		$(div).attr("style", "margin: 20px; padding: 10px; border: solid 1px blue;");
	}
	
	var label = document.createElement("label");
	$(label).attr("style", "margin-top: 20px; display: block;");
	$(label).html("Валидные промокоды:");
	div.appendChild(label);
	var textarea = document.createElement("textarea");
	$(textarea).attr("style", "width: 200px; height: 200px;");
	$(textarea).html(implode("\n", validPromocodes));
	div.appendChild(textarea);
	label = document.createElement("label");
	$(label).attr("style", "margin-top: 20px; display: block;");
	$(label).html("Невалидные промокоды:");
	div.appendChild(label);
	textarea = document.createElement("textarea");
	$(textarea).attr("style", "width: 200px; height: 200px;");
	$(textarea).html(implode("\n", invalidPromocodes));
	div.appendChild(textarea);
//	label = document.createElement("label");
//	$(label).attr("style", "margin-top: 20px; display: block;");
//	$(label).html("Истекшие промокоды:");
//	div.appendChild(label);
//	textarea = document.createElement("textarea");
//	$(textarea).attr("style", "width: 200px; height: 200px;");
//	$(textarea).html(implode("\n", expiredPromocodes));
//	div.appendChild(textarea);
	var button = document.createElement("input");
	$(button).attr("type", "button");
	$(button).attr("id", "btnNewTest");
	$(button).attr("style", "margin: 10px; padding: 5px; display: block;");
	$(button).val("Начать заново");
	div.appendChild(button);
	$(".checkout-new__head").get(0).appendChild(div);
	
	$("#btnNewTest").click(function() {
		startNewTest();
	});

}

function implode( glue, pieces ) {	// Join array elements with a string
	// 
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: _argos
	return ( ( pieces instanceof Array ) ? pieces.join ( glue ) : pieces );
}

function startNewTest() {
	$("#checkPromocodesContainer").html("");
	createAddPromocodesDialog();
//	createPromocodeTestingButton();
	$("#addPromocodesDialog").show();
//	$("#btnAddPromocodesToTest").hide();
	
//	$("#btnAddPromocodesToTest").click(function() {
//		console.log("add promocodes to test click");
//		$("#addPromocodesDialog").show();
//		$("#btnAddPromocodesToTest").hide();
//	});
	
	$("#btnStartTest").click(function() {
		var promocodes = $("#addingPromocodes").val();
		if(promocodes) {
			promocodesToTest = promocodes.split("\n");
			console.log("found " + promocodesToTest.length + " promocodes to test");
			startPromocodeTesting();
		}
	});
	
}

$(document).ready(function() {
	console.log("content js document ready");
	var savedState = localStorage.getItem("state");
	currentPromocodeIndex = parseInt(localStorage.getItem("currentPromocodeIndex"));
	var savedPromocodesToTest = localStorage.getItem("promocodesToTest");
	if(savedPromocodesToTest) {
		promocodesToTest = savedPromocodesToTest.split("\n");
	}
	var savedValidPromocodes = localStorage.getItem("validPromocodes");
	if(savedValidPromocodes) {
		validPromocodes = savedValidPromocodes.split("\n");
	}
	var savedInvalidPromocodes = localStorage.getItem("invalidPromocodes");
	if(savedInvalidPromocodes) {
		invalidPromocodes = savedInvalidPromocodes.split("\n");
	}
	var savedExpiredPromocodes = localStorage.getItem("expiredPromocodes");
	if(savedExpiredPromocodes) {
		expiredPromocodes = savedExpiredPromocodes.split("\n");
	}
	if(savedState == "finished") {
		handleAllPromocodesChecked();
	} else if(savedState == "running") {
		var div = document.createElement("div");
		$(div).attr("id", "checkPromocodesContainer");
		$(div).attr("style", "margin: 20px; padding: 10px; border: solid 1px blue;");
		$(".checkout-new__head").get(0).appendChild(div);
		
		var label = document.createElement("label");
		$(label).html("Предыдущая проверка была прервана! Проверено " + currentPromocodeIndex + " промокодов из " + promocodesToTest.length);
		$("#checkPromocodesContainer").get(0).appendChild(label);
		
		var button = document.createElement("input");
		$(button).attr("type", "button");
		$(button).attr("id", "btnContinueTest");
		$(button).attr("style", "margin: 10px; padding: 5px;");
		$(button).val("Продолжить");
		$("#checkPromocodesContainer").get(0).appendChild(button);
		
		button = document.createElement("input");
		$(button).attr("type", "button");
		$(button).attr("id", "btnNewTest");
		$(button).attr("style", "margin: 10px; padding: 5px;");
		$(button).val("Начать заново");
		$("#checkPromocodesContainer").get(0).appendChild(button);
		
		$("#btnContinueTest").click(function() {
			$("#checkPromocodesContainer").html("");
//			$("label[for='promocode']").click();
			setTimeout(checkPromocode1, 3000);
		});

		$("#btnNewTest").click(function() {
			startNewTest();
		});
	} else {
		var div = document.createElement("div");
		$(div).attr("id", "checkPromocodesContainer");
		$(div).attr("style", "margin: 20px; padding: 10px; border: solid 1px blue;");
		$(".checkout-new__head").get(0).appendChild(div);
		startNewTest();
	}
	
});
